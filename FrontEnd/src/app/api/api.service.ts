import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  API_URL = 'http://localhost:3000/login';

  constructor(private http: HttpClient) { }

  login(user){
    return this.http.post(this.API_URL, user);
  }

}
