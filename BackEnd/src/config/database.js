const sql = require('mssql');

const config = {
  user: 'ucr',
  password: '12345',
  server: 'localhost\\SQLEXPRESS',
  database: 'Northwind',
  options: {
    encrypt: true,
    enableArithAbort: true
  }
};

exports.config = config;
exports.sql = sql;
